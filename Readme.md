# 创建过程中所遇到的问题记录

## 可以参考的文档

<http://www.febeacon.com/lerna-docs-zh-cn/routes/commands/add.html#dev>

## 初始化

```bash
lerna init --independent // 添加参数后，lerna.json 中的版本会变成 independent ,表示每个 packages的包自助管理
```

## 创建一个包，name 包名，loc 位置可选(位置写法来源由于配置 workspaces)

```bash
lerna create <name> [loc]
```

## 安装模块，将所有 workspaces 中的模块安装到根目录下，进行使用

默认是 npm i,因为我们指定过 yarn，so,run yarn install,会把所有包的依赖安装到根 node_modules.

```bash
lerna bootstrap
lerna bootstrap --hoist
```

## 使用 --scope 给指定包安装模块(`lerna > 4`之后此指令不支持)

```bash
lerna add <packageName> --scope=eslint-config-some21

lerna add <packageName> --scope=eslint-config-some21 --dev
```

## 使用 pnpm 给指定包安装模块

```bash
pnpm add <packageName> --filter eslint-config-some21

pnpm add <packageName> -D --filter eslint-config-some21
```

## 列出下次发版 lerna publish 要更新的包

```bash
lerna changed
```

## 会打 tag，上传 git,上传 npm

```bash
lerna publish
```

## 其他

### 运行每个包的 script 命令

```bash
lerna run <script> -- [..args] # 运行所有包里面的有这个script的命令
```

### 运行任意命令在每个包

```bash
lerna exec -- < command > [..args] # runs the command in all packages
lerna exec -- rm -rf ./node_modules
lerna exec -- protractor conf.js

lerna exec --scope my-component -- ls -la
```

### 删除所有包的 node_modules 目录

```bash
lerna clean
```

### 列出所有的包，如果与你文夹里面的不符，进入那个包运行 yarn init -y 解决

```bash
lerna list
```

### 项目包建立软链，类似 npm link

```bash
lerna link
```

### 发布

```bash
lerna publish
```
