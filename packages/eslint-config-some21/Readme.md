# eslint-config-some21

> 大部分规则参考来源于 eslint-config-ali ,除 eslint 规则完全依据本人重新定义，其余规则参考 eslint-config-ali 规则后调整一定规则

需要注意：

- 请勿使用 0.0.23~0.0.24 版本，有点 bug。
- 请勿使用 0.1.7 版本，有点 bug。
- eslint 可以使用 7.x 也可以使用 8.x，但是请勿使用 eslint 9.x，因为 eslint 9 在配置的使用方式上，发生了变化，所以会导致 eslint 检测失效。

## 安装与使用

### 增加 typescript 的支持

> 如果需要启用 typescript 规则，需要额外安装（ `v0.1.4`版本以后内部已预装这两个插件）
>
> - `@typescript-eslint/eslint-plugin@6`
> - `@typescript-eslint/parser@6`

### 一般项目

#### 安装

```bash
cnpm i -D eslint@8
```

#### 使用

```json
/* Javascript */
{
  "extends": "some21"
}

/* Typescript */
{
  "extends": "some21/typescript"
}
```

### React 项目

#### 安装

```bash
cnpm i -D eslint@8 @babel/preset-react@7 eslint-plugin-react@7 eslint-plugin-react-hooks@4
```

#### 使用

```json
/* Javascript */
{
  "extends": "some21/react"
}

/* Typescript */
{
  "extends": "some21/typescript/react"
}
```

#### 增加 a11y 支持

```bash
# 安装
cnpm i -D eslint-plugin-jsx-a11y@6
```

##### 使用

```json
/* Javascript */
{
  "extends": ["some21/react", "some21/jsx-a11y"]
}

/* Typescript */
{
  "extends": ["some21/typescript/react", "some21/jsx-a11y"]
}
```

### Rax 项目

#### 安装

```bash
cnpm i -D eslint@8 @babel/preset-react@7 eslint-plugin-react@7 eslint-plugin-react-hooks@4 eslint-plugin-jsx-plus@0
```

#### 使用

```json
/* Javascript */
{
  "extends": "some21/rax"
}

/* Typescript */
{
  "extends": "some21/typescript/rax"
}
```

### Vue 项目

#### 安装

```bash
cnpm i -D eslint@8 eslint-plugin-vue@9 vue-eslint-parser@9
```

#### 使用

```json
/* Javascript */
{
  "extends": "some21/vue"
}

/* Typescript */
{
  "extends": "some21/typescript/vue"
}
```

### Node.js 项目

#### 安装

```bash
cnpm i -D eslint@8 eslint-config-egg@13
```

#### 使用

```json
/* Javascript */
{
  "extends": "some21/node"
}

/* Typescript */
{
  "extends": "some21/typescript/node"
}
```

### ES5 项目

#### 安装

无需额外安装

#### 使用

```json
{
  "extends": "some21/es5"
}
```

## 忽略规则的写法

### 禁用代码块

```js
/* eslint-disable */
consle.log("foo");
consle.log("bar");
/* eslint-disable */
```

### 禁用单行

```js
consle.log("foo"); // eslint-disable-line
```

### 禁用下一行

```js
// eslint-disable-next-line
console.log("foo");
```

### 禁用文件(放在代码最顶部)

```js
/* eslint-disable */
consle.log("foo");
consle.log("bar");
```
