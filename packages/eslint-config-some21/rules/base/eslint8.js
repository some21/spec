// http://eslint.cn/docs/rules/
// 这些规则在 eslint 8 之后才支持
module.exports = {
  rules: {
    "no-unused-private-class-members": "warn" /* 如果有未使用的私有类成员，则报错。disallow unused private class members */,
    "prefer-object-has-own":
      "error" /* 禁止使用 `Object.prototype.hasOwnProperty.call()`，在ES2022以后可以用`Object.hasOwn()`代替。 disallow use of `Object.prototype.hasOwnProperty.call()` and prefer use of `Object.hasOwn()` */,
    "require-atomic-updates": ["error", { allowProperties: true }] /* 禁止由于 await 或 yield的使用而可能导致出现竞态条件的赋值 */,
  },
};
