module.exports = {
  rules: {
    // "init-declarations":"error",/*要求或禁止 var 声明中的初始化 */
    "no-delete-var": "error" /* 禁止删除变量 */,
    // "no-label-var": "error" /* 不允许标签与变量同名 */,
    // "no-restricted-globals": "error" /* 禁用特定的全局变量 */,
    // "no-shadow": "error" /* 禁止变量声明与外层作用域的变量同名 */,
    "no-shadow-restricted-names": "error" /* 禁止将标识符定义为受限的名字 */,
    "no-undef": "error" /* 禁用未声明的变量，除非它们在 `global` 注释中被提到 */,
    "no-undef-init": "warn" /* 禁止将变量初始化为 undefined */,
    "no-undefined": "warn" /* 禁止将 undefined 作为标识符 */,
    "no-unused-vars": ["warn", { vars: "all", args: "after-used", ignoreRestSiblings: true, argsIgnorePattern: "(^_)|h|err", caughtErrors: "none" }] /*	禁止出现未使用过的变量 不检查函数未使用的参数 */,

    // "no-unused-vars": ["warn", { args: "none", argsIgnorePattern: "^_|(h$)", caughtErrors: "none", ignoreRestSiblings: true }] // 不检查函数未使用的参数
    // "no-use-before-define": "error" /*禁止在变量定义之前使用它们*/,
  },
};
