/**
 * 本文件继承了 egg-config-egg 的 node 规则，规则由 eslint-plugin-node 提供
 * @link https://github.com/eggjs/eslint-config-egg/blob/master/lib/rules/node.js
 * @link https://github.com/mysticatea/eslint-plugin-node
 */

module.exports = {
  extends: ["eslint-config-egg/lib/rules/node"],
};

// module.exports = {
//   env: {
//     commonjs: true /* CommonJS 全局变量和 CommonJS 作用域 (用于 Browserify/WebPack 打包的只在浏览器中运行的代码)。 */,
//     es6: true /* 启用除了 modules 以外的所有 ECMAScript 6 特性（该选项会自动设置 ecmaVersion 解析器选项为 6）。 */,
//     node: true /* Node.js 全局变量和 Node.js 作用域。 */,
//   },
//   rules: {
//     // "callback-return": "error" /*	强制数组方法的回调函数中有 return 语句 */,
//     // "global-require": "error" /*要求 require() 出现在顶层模块作用域中 */,
//     // "handle-callback-err": "error" /*要求回调函数中有容错处理 */,
//     // "no-buffer-constructor": "error" /* 不建议使用！！！ 禁用 Buffer() 构造函数 */,
//     // "no-mixed-requires": "error" /* 不建议使用！！！ 禁止混合常规变量声明和 require 调用 */,
//     // "no-new-require": "error" /* 不建议使用！！！禁止调用 require 时使用 new 操作符 */,
//     // "no-path-concat": "error" /* 不建议使用！！！禁止对 __dirname 和 __filename 进行字符串连接 */,
//     // "no-process-env": "error" /* 不建议使用！！！禁用 process.env */,
//     // "no-process-exit": "error" /* 不建议使用！！！禁用 process.exit() */,
//     // "no-restricted-modules": "error" /* 不建议使用！！！禁用通过 require 加载的指定模块 */,
//     // "no-sync": "error" /* 不建议使用！！！禁用同步方法 */
//   },
// };
