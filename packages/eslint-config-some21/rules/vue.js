/**
 * 本文件的规则由 eslint-plugin-vue 提供，使用 vue-eslint-parser 作为 parser
 * @link https://eslint.vuejs.org/rules/
 */
module.exports = {
  parser: "vue-eslint-parser",
  extends: ["plugin:@bidiao/vue3/base"],
  plugins: ["vue"],
  env: {
    browser: true /* 浏览器环境中的全局变量。 */,
    "shared-node-browser": true /* Node.js 和 Browser 通用全局变量。 */,
    es6: true,
  },
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: "module",
  },
  rules: {
    // 给 template 提供 eslint-disable 的能力，支持如下注释：
    // eslint-disable，eslint-enable，eslint-disable-line，eslint-disable-next-line
    "vue/comment-directive": "error",

    // 本条是对JS规约 no-unused-vars 的补充，防止变量被错误地标记为未使用
    "vue/jsx-uses-vars": "error",

    // 组件的 data 必须是一个函数
    "vue/no-shared-component-data": "error",

    // Prop 定义类型应该是构造函数
    "vue/require-prop-type-constructor": "error",

    // Prop 的默认值必须匹配它的类型
    "vue/require-valid-default-prop": "error",

    // 为 v-for 设置键值
    "vue/require-v-for-key": "error",

    // 避免 v-if 和 v-for 用在一起
    // 不允许在元素上同时使用 v-if 和v-for ，避免分不清，v-else 例外
    "vue/no-use-v-if-with-v-for": "error",

    // 计算属性禁止包含异步方法
    // 不允许异步的 computed 属性
    "vue/no-async-in-computed-properties": "error",

    // 禁止在对象字面量中出现重复的键
    "vue/no-dupe-keys": "error",

    // 禁止出现重复的属性
    "vue/no-duplicate-attributes": "error",

    // 禁止出现语法错误
    // @link https://html.spec.whatwg.org/multipage/parsing.html#parse-errors
    "vue/no-parsing-error": [
      "error",
      {
        "x-invalid-end-tag": false,
        "invalid-first-character-of-tag-name": false,
      },
    ],

    // 禁止使用 vue 中的关键字
    "vue/no-reserved-keys": "error",

    // 禁止在计算属性中对属性修改
    "vue/no-side-effects-in-computed-properties": "error",

    // 禁止 <template> 使用 key 属性
    "vue/no-template-key": "warn",

    // 禁止在 <textarea> 中出现 {{message}}
    "vue/no-textarea-mustache": "error",

    // 禁止注册没有使用的组件
    "vue/no-unused-components": "warn",

    // 禁止在 v-for 等指令或者 scope 中申明没有使用到的变量
    "vue/no-unused-vars": "warn",

    // <component> 必须有 v-bind:is
    "vue/require-component-is": "warn",

    // render 函数必须有返回值
    "vue/require-render-return": "error",

    // 计算属性必须有返回值
    "vue/return-in-computed-property": "error",

    // 强制在 v-on 指令使用 exact 修饰符，当同一个标签上有另一个带修饰符的 v-on 指令
    "vue/use-v-on-exact": "error",

    // 检查指令的合法性
    "vue/valid-template-root": "error",
    "vue/valid-v-bind": "error",
    "vue/valid-v-cloak": "error",
    "vue/valid-v-else-if": "error",
    "vue/valid-v-else": "error",
    "vue/valid-v-for": "error",
    "vue/valid-v-html": "error",
    "vue/valid-v-if": "error",
    "vue/valid-v-model": "error",
    "vue/valid-v-on": "error",
    "vue/valid-v-once": "error",
    "vue/valid-v-pre": "error",
    "vue/valid-v-show": "error",

    //#region vue html 的样式控制
    // 接管 vue 文件中html的对齐方式
    "vue/html-indent": ["error", 2, { alignAttributesVertically: true }],
    // vue html 中 属性的 = 前后不允许有空格
    "vue/no-spaces-around-equal-signs-in-attribute": "error",
    // vue html 中的标签闭合写法，无论是单行还是多行，最后的 > 都不必换行
    "vue/html-closing-bracket-newline": ["error", { singleline: "never", multiline: "never" }],
    // 组件始终有结束标签，自定义组件不强制要求
    "vue/html-self-closing": ["error", { html: { void: "always", normal: "never", component: "any" } }],
    // 强制要求每行的最大属性数
    "vue/max-attributes-per-line": ["error", { singleline: 1, multiline: 1 }],
    // 强制实施第一个属性的位置。不允许在第一个属性之前使用换行符
    "vue/first-attribute-linebreak": ["error", { singleline: "beside", multiline: "beside" }],
    // 不需要写子成员时写了子成员则提示错误
    "vue/no-child-content": "error",
    // 针对属性的排序进行了控制
    "vue/attributes-order": [
      "error",
      {
        order: [
          "CONDITIONALS" /* e.g. 'v-if', 'v-else-if', 'v-else', 'v-show', 'v-cloak' */,
          "SLOT" /* e.g. 'v-slot', 'slot'. */,
          "LIST_RENDERING" /* e.g. 'v-for item in items' */,
          "UNIQUE" /* e.g. 'ref', 'key' */,
          ["DEFINITION", "GLOBAL"] /* e.g. 'is', 'v-is'   // e.g. 'id' */,
          "OTHER_ATTR" /* e.g. 'custom-prop="foo"', 'v-bind:prop="foo"', ':prop="foo"' */,
          "EVENTS" /* e.g. '@click="functionCall"', 'v-on="event"' */,
          "OTHER_DIRECTIVES" /* e.g. 'v-custom-directive' */,
          "RENDER_MODIFIERS" /* e.g. 'v-once', 'v-pre' */,
          "TWO_WAY_BINDING" /* e.g. 'v-model' */,
          "CONTENT" /* e.g. 'v-text', 'v-html' */,
        ],
        alphabetical: true, // 字母排序
      },
    ],
    // 不能使用多余的空格内容
    "vue/no-multi-spaces": ["error", { ignoreProperties: false }],
    "vue/singleline-html-element-content-newline": "off",
    "vue/multiline-html-element-content-newline": "off",
    // 针对{{aaa}}的格式化 https://eslint.vuejs.org/rules/mustache-interpolation-spacing.html
    "vue/mustache-interpolation-spacing": "error",
    //! Deprecated :即将废弃。不允许在绑定事件上 写执行方法
    "vue/v-on-function-call": ["error", "never"],
    // 替代 vue/v-on-function-call 。使用内联写法时候， 需要 `()=>handle() /* commet */` 的写法
    "vue/v-on-handler-style": [
      "error",
      ["inline-function", "method"], // 顺序不能发生变化，否则fix 将引起错误
      {
        ignoreIncludesComment: false,
      },
    ],
    // 不允许在v-model中使用自定义的 modifers 操作
    "vue/no-custom-modifiers-on-v-model": "error",
    // html 中必须使用双引号
    "vue/html-quotes": "warn",
    // html 中使用template 时必须配合条件使用
    "vue/no-lone-template": "warn",
    // 作为块级标签的元素必须都是出现在单行
    "vue/block-tag-newline": ["warn", { singleline: "always", multiline: "always", maxEmptyLines: 1 /* 内部最大允许的空白行 */ }],
    // 让 vue 文件中的script 和 template 之间有一个空行
    "vue/padding-line-between-blocks": "warn",
    "vue/script-indent": ["error", 2, { baseIndent: 1, switchCase: 1 }] /* vue 文件中的script 缩进设置 */,
    // html 的标签全部使用 PascalCase 写法，不管组件有没有注册过
    "vue/component-name-in-template-casing": ["error", "kebab-case", { registeredComponentsOnly: false }],
    // 在component 中声明组件的时候，名称也为 PascalCase 写法
    "vue/component-options-name-casing": ["error", "kebab-case"],
    //#endregion

    // 不允许出现重复的判断条件
    "vue/no-dupe-v-else-if": "error",
    // template 中不允许使用this，默认为 never
    "vue/this-in-template": "error",

    "vue/arrow-spacing": "error",
    // 禁止使用 == 号 比较时必须完全匹配 使用 ===
    "vue/eqeqeq": ["error", "always"],

    // 在关键字附近要有一个空格
    "vue/keyword-spacing": "warn",
    // 运算符左右需要有空格
    "vue/space-infix-ops": "warn",

    //#region Vue3.0
    // 仅允许 is 的写法出现在 components 中
    "vue/no-deprecated-html-element-is": "warn",
    //#endregion
  },
};
