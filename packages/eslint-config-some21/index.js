// http://eslint.cn/docs/rules/
module.exports = {
  env: {
    commonjs: true /* CommonJS 全局变量和 CommonJS 作用域 (用于 Browserify/WebPack 打包的只在浏览器中运行的代码)。 */,
    es6: true /* 启用除了 modules 以外的所有 ECMAScript 6 特性（该选项会自动设置 ecmaVersion 解析器选项为 6）。 */,
    node: true /* Node.js 全局变量和 Node.js 作用域。 */,
  },
  extends: ["./rules/base/best-practices", "./rules/base/possible-errors", "./rules/base/styles", "./rules/base/variables", "./rules/base/es6", "./rules/imports"].map(require.resolve),
  parser: "@babel/eslint-parser",
  parserOptions: {
    requireConfigFile: false,
    ecmaVersion: 2020,
    sourceType: "module", // typescript 所需？
    ecmaFeatures: {
      globalReturn: false,
      impliedStrict: true,
      jsx: true,
    },
  },
  root: true,
};
