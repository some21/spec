module.exports = {
  extends: ["./index", "../rules/vue"].map(require.resolve),
  parserOptions: {
    // https://github.com/mysticatea/vue-eslint-parser#parseroptionsparser
    // parser: "@typescript-eslint/parser",
    parser: {
      js: "espree",
      jsx: "espree",
      cjs: "espree",
      mjs: "espree",

      ts: "@typescript-eslint/parser",
      tsx: "@typescript-eslint/parser",
      cts: "@typescript-eslint/parser",
      mts: "@typescript-eslint/parser",

      // Leave the template parser unspecified, so that it could be determined by `<script lang="...">`
    },
    extraFileExtensions: [".vue"],
  },
  settings: {
    // vue ts 在引入带别名的模块过程中，会出现 import/no-unresolved 错误，此处配置project可以解决
    "import/resolver": {
      typescript: {
        alwaysTryTypes: true,
        project: ["**/tsconfig.json", "**/tsconfig.*.json"],
      },
    },
  },
  overrides: [
    {
      files: ["*.vue"],
      rules: {
        indent: "off",
        "@typescript-eslint/indent": "off",
        // "@typescript-eslint/explicit-module-boundary-types": "off", // 要求对导出的函数和类的公共类方法进行显式返回和参数类型

        // The core 'no-unused-vars' rules (in the eslint:recommeded ruleset)
        // does not work with type definitions
        "no-unused-vars": "off",
        // Disable `no-undef` rule within TypeScript files because it incorrectly errors when exporting default interfaces
        // https://github.com/iamturns/eslint-config-airbnb-typescript/issues/50
        // This will be caught by TypeScript compiler if `strictNullChecks` (or `strict`) is enabled

        //! 虽然在tsconfig 中添加了 `strictNullChecks` 但是好像并没有生效，所以这里还是恢复检测
        "no-undef": "error",
      },
    },
  ],
};
