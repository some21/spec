module.exports = {
  extends: ["./rules/base/best-practices", "./rules/base/possible-errors", "./rules/base/styles", "./rules/base/variables", "./rules/es5"].map(require.resolve),
  root: true,
};
